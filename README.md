# User IP

Application that displays user's IPs and count how many times they hit this app.

## Endpoint

`http://application_address/`

Example:
```bash
$ curl http://192.168.1.1/
User address : 172.172.1.150
Hits: 1

$ curl http://192.168.1.1/
User address : 172.172.1.150
Hits: 2

$ curl http://192.168.1.1/
User address : 172.172.1.150
Hits: 3

```
## Usage

All usage of this application is done through `make` command.

### Running on local machine without docker

1. Build the application
```bash
$ make build
```

2. Run the application
```bash
$ make run
```
### Running on local machine with docker

1. Build the application
```bash
$ make build-linux
```

2. Build docker image
```bash
$ make build-docker
```

3. Run application via docker compose
```bash
$ make run-compose
```

4. Stop application via docker compose
```bash
$ make stop-compose
```

5. (Optional) Push docker images to docker hub
```bash
$ make push docker
```