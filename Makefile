REPO = cikupin
TAG = latest

.PHONY: build
build:
	go build -o bin/user-ip

.PHONY: build-linux
build-linux:
	GOOS=linux GOARCH=amd64 go build -o bin/user-ip

.PHONY: run
run:
	./bin/user-ip

.PHONY: build-docker
build-docker: build-linux
	docker build -t ${REPO}/user-ip:${TAG} .

.PHONY: push-docker
push-docker: build-docker
	docker push ${REPO}/user-ip:${TAG}

.PHONY: run-compose
run-compose:
	REPO=${REPO} TAG=${TAG} docker-compose up -d

.PHONY: stop-compose
stop-compose:
	REPO=${REPO} TAG=${TAG} docker-compose stop

.PHONY: remove-compose
remove-compose:
	REPO=${REPO} TAG=${TAG} docker-compose stop