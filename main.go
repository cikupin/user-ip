package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
)

var userData map[string]int

func userIP(w http.ResponseWriter, r *http.Request) {
	ip := r.RemoteAddr
	var cleanIP string

	if strings.Contains(ip, ":") {
		cleanIP = net.ParseIP(strings.Split(ip, ":")[0]).String()
	} else {
		cleanIP = net.ParseIP(ip).String()
	}

	if _, ok := userData[cleanIP]; ok {
		userData[cleanIP]++
	} else {
		userData[cleanIP] = 1
	}

	result := fmt.Sprintf("User address : %s\nHits: %d\n", cleanIP, userData[cleanIP])
	fmt.Print(result)

	w.WriteHeader(200)
	w.Write([]byte(result))
}

func main() {
	userData = make(map[string]int)

	mux := http.NewServeMux()
	mux.HandleFunc("/", userIP)

	srv := &http.Server{
		Addr:    ":80",
		Handler: mux,
	}

	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, os.Interrupt)
		<-sigint

		if err := srv.Shutdown(context.Background()); err != nil {
			log.Printf("HTTP server Shutdown: %v", err)
		}
		close(idleConnsClosed)
	}()

	fmt.Println("[I] Application is running on port 80...")
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		log.Fatalf("HTTP server ListenAndServe: %v", err)
	}

	<-idleConnsClosed
	fmt.Println("[X] Application is closed...")
}
