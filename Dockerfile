FROM alpine:3.15.4

COPY bin/user-ip /opt/user-ip

EXPOSE 80
ENTRYPOINT ["/opt/user-ip"]